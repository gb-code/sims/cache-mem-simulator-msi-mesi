#include <stdlib.h>
#include <stdlib.h>
#include <assert.h>
#include <fstream>
#include <sstream>
#include <string>
#include <istream>
#include <vector>
#include <iomanip>                
#include "cache.h"
//#include "mesi.h"
using namespace std;





void MESI(vector <Cache*> ArrayOfCacheInProcessors, int processor_name, ulong address, char ReadWriteOperation, int num_processor)
{
	//For INVALID State
	if (ArrayOfCacheInProcessors[processor_name]->findLine(address) == NULL)
	{	//PrRd --- For Read(C) : INVALID ---> SHARED | For Read(!C) : INVALID ---> EXCLUSIVE | FOR Write : INVALID ---> MODIFIED
		bool C = false;
		for (int i = 0; i<num_processor; i++)
		{
			if (i != processor_name && ArrayOfCacheInProcessors[i]->findLine(address) != NULL)
			{
				C = true;
			}
		}
		//--------------------------------------------------Read Operation-----------------------------------------------
		//Other Processor Read on multi-core system
		if (ReadWriteOperation == 'r')
		{
			if (C == true)
			{
				ArrayOfCacheInProcessors[processor_name]->Access(address, ReadWriteOperation);
				(ArrayOfCacheInProcessors[processor_name]->findLine(address))->setFlags(SHARED);
				//Cache to Cache Transfer when Line exists in another processor_namecessor and State INVALID-->SHARED
				ArrayOfCacheInProcessors[processor_name]->num_of_cache_to_cache_transfer++;
				//Generate BusRD to turn modified to shared
				for (int i = 0; i<num_processor; i++)
				{
					if (i != processor_name && ArrayOfCacheInProcessors[i]->findLine(address) != NULL)
					{
						if ((ArrayOfCacheInProcessors[i]->findLine(address))->getFlags() == MODIFIED)
						{
							ArrayOfCacheInProcessors[i]->num_of_interventions++;
							ArrayOfCacheInProcessors[i]->num_of_flushes++;
							ArrayOfCacheInProcessors[i]->writeBack(address);
							//ArrayOfCacheInProcessors[processor_name]->num_of_chache_to_cache_transfer++;
						}
						else if ((ArrayOfCacheInProcessors[i]->findLine(address))->getFlags() == EXCLUSIVE)
						{
							ArrayOfCacheInProcessors[i]->num_of_interventions++;
							//ArrayOfCacheInProcessors[processor_name]->num_of_chache_to_cache_transfer++;

						}
						(ArrayOfCacheInProcessors[i]->findLine(address))->setFlags(SHARED);

					}
				}
			}
			else
			{
				ArrayOfCacheInProcessors[processor_name]->Access(address, ReadWriteOperation);
				(ArrayOfCacheInProcessors[processor_name]->findLine(address))->setFlags(EXCLUSIVE);
			}
			return;
		}
		//--------------------------------------------------Write Operation-----------------------------------------------
		else
		{	//Invalid ---> Modified
			ArrayOfCacheInProcessors[processor_name]->Access(address, ReadWriteOperation);
			(ArrayOfCacheInProcessors[processor_name]->findLine(address))->setFlags(MODIFIED);
			//Generates BusRDx to invalidate other caches
			for (int i = 0; i<num_processor; i++)
			{
				if (i != processor_name && ArrayOfCacheInProcessors[i]->findLine(address) != NULL)
				{
					if ((ArrayOfCacheInProcessors[i]->findLine(address))->getFlags() == SHARED)
					{
						ArrayOfCacheInProcessors[i]->num_of_invalidations++;
						ArrayOfCacheInProcessors[processor_name]->num_of_cache_to_cache_transfer++;
					}
					else if ((ArrayOfCacheInProcessors[i]->findLine(address))->getFlags() == MODIFIED)
					{
						ArrayOfCacheInProcessors[i]->num_of_flushes++;
						ArrayOfCacheInProcessors[i]->writeBack(address);
						ArrayOfCacheInProcessors[i]->num_of_invalidations++;
						ArrayOfCacheInProcessors[processor_name]->num_of_cache_to_cache_transfer++;
					}
					else
					{
						ArrayOfCacheInProcessors[i]->num_of_invalidations++;

					}
					(ArrayOfCacheInProcessors[i]->findLine(address))->invalidate();
				}
			}
			return;
		}
	}
	//For Exclusive State
	switch(ArrayOfCacheInProcessors[processor_name]->findLine(address)->getFlags())
	{
		case EXCLUSIVE:
						//PrRd --- For Read : EXCLUSIVE ---> EXCLUSIVE | FOR Write : EXCLUSIVE ---> MODIFIED
						//--------------------------------------------------Read Operation-----------------------------------------------
						if (ReadWriteOperation == 'r')
						{
							ArrayOfCacheInProcessors[processor_name]->Access(address, ReadWriteOperation);
							(ArrayOfCacheInProcessors[processor_name]->findLine(address))->setFlags(EXCLUSIVE);
							return;
						}
						//--------------------------------------------------Write Operation-----------------------------------------------
						else
						{	//EXCLUSIVE ---> MODIFIED
							ArrayOfCacheInProcessors[processor_name]->Access(address, ReadWriteOperation);
							(ArrayOfCacheInProcessors[processor_name]->findLine(address))->setFlags(MODIFIED);
							return;
						}
						break;
		case SHARED:
						//--------------------------------------------------Read Operation-----------------------------------------------		
						if (ReadWriteOperation == 'r')
						{
							ArrayOfCacheInProcessors[processor_name]->Access(address, ReadWriteOperation);
							return;
						}
						/*WRITE*/
						//--------------------------------------------------Write Operation-----------------------------------------------
						else
						{	//Shared  ----> Modified
							ArrayOfCacheInProcessors[processor_name]->Access(address, ReadWriteOperation);
							(ArrayOfCacheInProcessors[processor_name]->findLine(address))->setFlags(MODIFIED);
							//Generates BusUPGR to invalidate other caches
							for (int i = 0; i<num_processor; i++)
							{
								if (i != processor_name && ArrayOfCacheInProcessors[i]->findLine(address) != NULL)
								{
									if ((ArrayOfCacheInProcessors[i]->findLine(address))->getFlags() == SHARED)
									{
										ArrayOfCacheInProcessors[i]->num_of_invalidations++;
										//ArrayOfCacheInProcessors[i]->num_of_chache_to_cache_transfer++;
									}
									else
									{
										ArrayOfCacheInProcessors[i]->num_of_invalidations++;
									}
									(ArrayOfCacheInProcessors[i]->findLine(address))->invalidate();
								}
							}
							return;
						}
						break;
		case MODIFIED:
						//--------------------------------------------------Read Operation-----------------------------------------------
						if (ReadWriteOperation == 'r')
						{
							ArrayOfCacheInProcessors[processor_name]->Access(address, ReadWriteOperation);
							(ArrayOfCacheInProcessors[processor_name]->findLine(address))->setFlags(MODIFIED);
							return;
						}
						//--------------------------------------------------Write Operation-----------------------------------------------
						else
						{
							ArrayOfCacheInProcessors[processor_name]->Access(address, ReadWriteOperation);
							(ArrayOfCacheInProcessors[processor_name]->findLine(address))->setFlags(MODIFIED);
							return;
						}
						break;
		default:		cout<<"\n \n This is the default of case of state transitions!!";
						
	}
	
	return;
}
