Cache_coherence_protocol
========================

C++ Cache Memory Simulator implementing MSI and MESI coherence protocols. The design of simulator is easy to add more coherence protocols.

RUN Steps:
========================

1) make clean

2) make

3) ./smp_cache cacheSize assoc blockSize numProc protocol traceFile

RUN Configurations
========================

1) cacheSize = 512Kb(512000), 1024Kb(1024000)

2) assoc(associativity) = 2 or 4 or 8

3) blockSize = 64 or 32

4) numProc = 4 or 8

5) protocol = 0(MSI), 1(MESI)

traceFile = canneal.04t.debug(10k entries) or canneal.04t.longTrace(500k entries) // TraceFile reads (processor_name, ReadWrite operation, Address)
