#include <stdlib.h>
#include <stdlib.h>
#include <assert.h>
#include <fstream>
#include <sstream>
#include <string>
#include <istream>
#include <vector>
#include <iomanip>                
#include "cache.h"
//#include "mesi.h"
using namespace std;

void MSI(vector <Cache*> ArrayOfCacheInProcessors, int processor_name, ulong address, char ReadWriteOperation, int num_processor)
{
	//For NULL State
	if (ArrayOfCacheInProcessors[processor_name]->findLine(address) == NULL)
	{	//PrRd --- For Read : NULL ---> SHARED | FOR Write : NULL ---> MODIFIED
		
		//--------------------------------Read Operation------------------------------------
		if (ReadWriteOperation == 'r')
		{
			ArrayOfCacheInProcessors[processor_name]->Access(address, ReadWriteOperation);
			//ArrayOfCacheInProcessors[processor_name]->num_of_mem_trans++;
			ArrayOfCacheInProcessors[processor_name]->findLine(address)->setFlags(SHARED);
			//Generate BusRD to turn modified to shared
			for (int i = 0; i<num_processor; i++)
			{
				if (i != processor_name && ArrayOfCacheInProcessors[i]->findLine(address) != NULL)
				{
					if ((ArrayOfCacheInProcessors[i]->findLine(address))->getFlags() == MODIFIED)
					{
						ArrayOfCacheInProcessors[i]->num_of_flushes++;
						ArrayOfCacheInProcessors[i]->writeBack(address);
						ArrayOfCacheInProcessors[i]->num_of_interventions++;
						//ArrayOfCacheInProcessors[i]->num_of_mem_trans++;
						(ArrayOfCacheInProcessors[i]->findLine(address))->setFlags(SHARED);
					}
				}
			}
			return;
		}
		//--------------------------------Write Operation------------------------------------
		else
		{	//NULL ---> Modified
			ArrayOfCacheInProcessors[processor_name]->Access(address, ReadWriteOperation);
			(ArrayOfCacheInProcessors[processor_name]->findLine(address))->setFlags(MODIFIED);
			//ArrayOfCacheInProcessors[processor_name]->num_of_mem_trans++;
			//Generates BusRDx to invalidate other caches
			for (int i = 0; i<num_processor; i++)
			{
				if (i != processor_name && ArrayOfCacheInProcessors[i]->findLine(address) != NULL)
				{
					if((ArrayOfCacheInProcessors[i]->findLine(address)->getFlags())==MODIFIED)
					{
						//ArrayOfCacheInProcessors[i]->num_of_mem_trans++;
					}
					(ArrayOfCacheInProcessors[i]->findLine(address))->invalidate();
					ArrayOfCacheInProcessors[i]->num_of_invalidations++;
					ArrayOfCacheInProcessors[i]->num_of_flushes++;
					ArrayOfCacheInProcessors[i]->writeBack(address);
					//ArrayOfCacheInProcessors[i]->num_of_mem_trans++;
				}
			}
			return;
		}
	}
	
	else
        {
            switch(ArrayOfCacheInProcessors[processor_name]->findLine(address)->getFlags())
            {
                case INVALID:
                                //PrRd --- For Read : INVALID ---> SHARED | FOR Write : INVALID ---> MODIFIED
								//--------------------------------Read Operation------------------------------------
                                if (ReadWriteOperation == 'r')
                                {
                                        ArrayOfCacheInProcessors[processor_name]->Access(address, ReadWriteOperation);
                                        (ArrayOfCacheInProcessors[processor_name]->findLine(address))->setFlags(SHARED);
                                        //ArrayOfCacheInProcessors[processor_name]->num_of_mem_trans++;
                                        for (int i = 0; i<num_processor; i++)
                                        {
                                                if (i != processor_name && ArrayOfCacheInProcessors[i]->findLine(address) != NULL)
                                                {
                                                        if ((ArrayOfCacheInProcessors[i]->findLine(address))->getFlags() == MODIFIED)
                                                        {
                                                                ArrayOfCacheInProcessors[i]->num_of_flushes++;
                                                                ArrayOfCacheInProcessors[i]->writeBack(address);
                                                                ArrayOfCacheInProcessors[i]->num_of_interventions++;
                                                                //ArrayOfCacheInProcessors[i]->num_of_mem_trans++;
                                                                (ArrayOfCacheInProcessors[i]->findLine(address))->setFlags(SHARED);
                                                        }
                                                        //(ArrayOfCacheInProcessors[i]->findLine(address))->setFlags(SHARED);

                                                }
                                        }
                                        return;
                                }
								//------------------------------------Write Operation------------------------------------
                                else
                                {	//Invalid ---> Modified
                                        ArrayOfCacheInProcessors[processor_name]->Access(address, ReadWriteOperation);
                                        (ArrayOfCacheInProcessors[processor_name]->findLine(address))->setFlags(MODIFIED);
                                        //ArrayOfCacheInProcessors[processor_name]->num_of_mem_trans++;
                                        //Generates BusRDx to invalidate other caches
                                        for (int i = 0; i<num_processor; i++)
                                        {
                                                if (i != processor_name && ArrayOfCacheInProcessors[i]->findLine(address) != NULL)
                                                {
                                                        if((ArrayOfCacheInProcessors[i]->findLine(address)->getFlags())==MODIFIED)
                                                        {
                                                                //ArrayOfCacheInProcessors[i]->num_of_mem_trans++;
                                                        }
                                                        (ArrayOfCacheInProcessors[i]->findLine(address))->invalidate();
                                                        ArrayOfCacheInProcessors[i]->num_of_invalidations++;	
                                                }
                                        }
                                        return;
                                }
                                break;
                                
                case SHARED:
                                //--------------------------------Read Operation------------------------------------
								if (ReadWriteOperation == 'r')
                                {
                                        ArrayOfCacheInProcessors[processor_name]->Access(address, ReadWriteOperation);
                                        return;
                                }
                                //--------------------------------Write Operation------------------------------------      
                                else
                                {	//Shared  ----> Modified
                                        ArrayOfCacheInProcessors[processor_name]->Access(address, ReadWriteOperation);
                                        (ArrayOfCacheInProcessors[processor_name]->findLine(address))->setFlags(MODIFIED);
                                        ArrayOfCacheInProcessors[processor_name]->num_of_mem_trans++;
                                        //Generates BusRDx to invalidate other caches
                                        for (int i = 0; i<num_processor; i++)
                                        {
                                                if (i != processor_name && ArrayOfCacheInProcessors[i]->findLine(address) != NULL)
                                                {
                                                        if((ArrayOfCacheInProcessors[i]->findLine(address)->getFlags())!=INVALID)
                                                        {
                                                                ArrayOfCacheInProcessors[i]->num_of_invalidations++;
                                                            (ArrayOfCacheInProcessors[i]->findLine(address))->invalidate();
                                                        }
                                                }
                                        }
                                        return;
                                }
                                break;
                                
                case MODIFIED:
                                //--------------------------------Read Operation------------------------------------
								if (ReadWriteOperation == 'r')
                                {
                                        ArrayOfCacheInProcessors[processor_name]->Access(address, ReadWriteOperation);
                                        (ArrayOfCacheInProcessors[processor_name]->findLine(address))->setFlags(MODIFIED);
                                        return;
                                }
								//--------------------------------Write Operation------------------------------------
                                else
                                {
                                        ArrayOfCacheInProcessors[processor_name]->Access(address, ReadWriteOperation);
                                        //ArrayOfCacheInProcessors[processor_name]->num_of_mem_trans++;
                                        (ArrayOfCacheInProcessors[processor_name]->findLine(address))->setFlags(MODIFIED);
                                        return;
                                }
                                break;
                                
                default:    cout<<"i shouldn't get here, error condition!!!";break;
            }//case ends here
            
       
	
	
        }//else of NULL ends here
	
	

    return;
}
