#include <stdlib.h>
#include <stdlib.h>
#include <assert.h>
#include <fstream>
#include <sstream>
#include <string>
#include <istream>
#include <vector>
#include <iomanip>                
#include "cache.h"

using namespace std;



//call to protocol functions

void MSI(vector <Cache*> ArrayOfCacheInProcessors, int processor_name, ulong address, char ReadWriteOperation, int num_processor);
void MESI(vector <Cache*> ArrayOfCacheInProcessors, int processor_name, ulong address, char ReadWriteOperation, int num_processor);


int main(int argc, char *argv[])
{
	
	ifstream fin;
	FILE * pFile;

    int processor_name;
    uchar ReadWriteOperation;
    uint addr;

	if(argv[1] == NULL){
		 printf("input format: ");
		 printf("./smp_cache <cache_size> <assoc> <block_size> <num_processor> <protocol> <trace_file> \n");
		 exit(0);
        }

	
	int cache_size = atoi(argv[1]);
	int cache_assoc= atoi(argv[2]);
	int blk_size   = atoi(argv[3]);
	int num_processor = atoi(argv[4]);/*1, 2, 4, 8*/
	int protocol   = atoi(argv[5]);	 /*0:MSI, 1:MESI*/
	char *fname =  (char *)malloc(20);
 	fname = argv[6];
        
        vector <Cache*> ArrayOfCacheInProcessors;
	//iterate over the processors to fetch the cache configuration
        //Step - make the cache object per processor
	for (int i = 0; i< num_processor; i++)
	{
		Cache* cache_instance = new Cache(cache_size, cache_assoc, blk_size);
		ArrayOfCacheInProcessors.push_back(cache_instance);
	}
	

	pFile = fopen(fname,"r");
	
	if(pFile == 0)
	{   
		printf("Trace file \n");
		exit(0);
	}

    switch(protocol)
    {
                //**read trace file,line by line,each(processor_namecessor#,ReadWriteOperationeration,address)**//
                //*****processor_namepagate each request down through memory hierarchy**********//
                //*****by calling ArrayOfCacheInProcessors[processor_namecessor#]->Access(...)***************//
        case 0:
                while(!feof(pFile))
                {
                        fscanf(pFile,"%d %c %x\n",&processor_name,&ReadWriteOperation,&addr);
                MSI(ArrayOfCacheInProcessors, processor_name, addr, ReadWriteOperation, num_processor);
                }
                
                fclose(pFile);

                
                //print out all caches' statistics 
                for (int i = 0; i<num_processor; i++)
                {
                        cout << "============ Simulation results for MSI protocol (Cache of Processor -" << i << ") ============" << endl;
                        cout << "01. number of reads:                " << ArrayOfCacheInProcessors[i]->getReads() << endl;
                        cout << "02. number of read misses:          " << ArrayOfCacheInProcessors[i]->getRM() << endl;
                        cout << "03. number of writes:               " << ArrayOfCacheInProcessors[i]->getWrites() << endl;
                        cout << "04. number of write misses:         " << ArrayOfCacheInProcessors[i]->getWM() << endl;
                        cout << "05. total miss rate:                " << fixed << setprecision(2)<< (ArrayOfCacheInProcessors[i]->getWM()+ArrayOfCacheInProcessors[i]->getRM()) * 100.0 / (ArrayOfCacheInProcessors[i]->getReads()+ArrayOfCacheInProcessors[i]->getWrites())<< '%' << endl;
                        cout << "06. number of write backs:          " << ArrayOfCacheInProcessors[i]->getWB() << endl;
                        cout << "07. number of cache to cache transfers:       " << ArrayOfCacheInProcessors[i]->num_of_cache_to_cache_transfer << endl;
                        cout << "08. number of memory transactions:      " << ArrayOfCacheInProcessors[i]->num_of_mem_trans+ArrayOfCacheInProcessors[i]->getRM()+ArrayOfCacheInProcessors[i]->getWM()+ArrayOfCacheInProcessors[i]->getWB()<< endl;
                        cout << "09. number of interventions:            " << ArrayOfCacheInProcessors[i]->num_of_interventions << endl;
                        cout << "10. number of invalidations:            " << ArrayOfCacheInProcessors[i]->num_of_invalidations << endl;
                        cout << "11. number of flushes:              " << ArrayOfCacheInProcessors[i]->num_of_flushes << endl;
                }
                return 0;
                
                break;
        case 1:
                while(!feof(pFile))
                {
                        fscanf(pFile,"%d %c %x\n",&processor_name,&ReadWriteOperation,&addr);
                MESI(ArrayOfCacheInProcessors, processor_name, addr, ReadWriteOperation, num_processor);
                }

                fclose(pFile);
                
                //print out all caches' statistics 
                for (int i = 0; i<num_processor; i++)
                {
                        cout << "============ Simulation results for MESI protocol (Cache of Processor - " << i << ") ============" << endl;
                        cout << "01. number of reads:                " << ArrayOfCacheInProcessors[i]->getReads() << endl;
                        cout << "02. number of read misses:          " << ArrayOfCacheInProcessors[i]->getRM() << endl;
                        cout << "03. number of writes:               " << ArrayOfCacheInProcessors[i]->getWrites() << endl;
                        cout << "04. number of write misses:         " << ArrayOfCacheInProcessors[i]->getWM() << endl;
                        cout << "05. total miss rate:                " << fixed << setprecision(2)<< (ArrayOfCacheInProcessors[i]->getWM()+ArrayOfCacheInProcessors[i]->getRM()) * 100.0 / (ArrayOfCacheInProcessors[i]->getReads()+ArrayOfCacheInProcessors[i]->getWrites())<< '%' << endl;
                        cout << "06. number of write backs:          " << ArrayOfCacheInProcessors[i]->getWB() << endl;
                        cout << "07. number of cache to cache transfers:      " << ArrayOfCacheInProcessors[i]->num_of_cache_to_cache_transfer << endl;
                        cout << "08. number of memory transactions:      " << ArrayOfCacheInProcessors[i]->getWM() + ArrayOfCacheInProcessors[i]->getRM() + ArrayOfCacheInProcessors[i]->getWB() - ArrayOfCacheInProcessors[i]->num_of_cache_to_cache_transfer << endl;
                        cout << "09. number of interventions:            " << ArrayOfCacheInProcessors[i]->num_of_interventions << endl;
                        cout << "10. number of invalidations:            " << ArrayOfCacheInProcessors[i]->num_of_invalidations << endl;
                        cout << "11. number of flushes:              " << ArrayOfCacheInProcessors[i]->num_of_flushes << endl;
                }
                return 0;
                
                break;
        default:
                cout<<"\n This is default condition, I shouldn't get here!!!";break;
    }
    
    
	
}